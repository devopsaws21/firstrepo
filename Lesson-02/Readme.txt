Project : Deploy the files to Bitbucket via Git.



DESCRIPTION :



Create a directory named “Lesson-02” and perform the following:



• Create multiple files with .html, .xml, .txt, .py, .js, and .java.



• Add random content in each file.



• Connect git local to Bitbucket remote.



• Push the directory “Lesson-02” to Bitbucket account.



Access: Click on the Labs tab on the left side panel of the LMS.

Copy or note the username and password that is generated. Click on the Launch Lab button.

On the page that appears, enter the username and password in the respective fields, and click Login.


